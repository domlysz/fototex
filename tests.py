# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
from fototex.foto import Foto

# foto = Foto("/home/benjamin/Documents/PRO/PRODUITS/SENTINEL"
#             "/S2A_MSIL1C_20201006T132241_N0209_R038_T23LKC_20201006T151824.SAFE/GRANULE"
#             "/L1C_T23LKC_A027634_20201006T132239/IMG_DATA/T23LKC_20201006T132241_B04.jp2")

# root = tkinter.Tk()
# plot(root, foto.dataset, foto.band, reduced_r_spectra, 0.6, 16, "max", 3, [2, 98])
# tkinter.mainloop()

foto = Foto("/home/benjamin/Documents/PRO/PRODUITS/SENTINEL"
            "/S2A_MSIL1C_20201006T132241_N0209_R038_T23LKC_20201006T151824.SAFE/GRANULE"
            "/L1C_T23LKC_A027634_20201006T132239/IMG_DATA/clip.tif", method="moving")

foto.run(19, window_step=2, keep_dc_component=True)
foto.out_dir = "/home/benjamin/Documents/PRO/PRODUITS/FOTO_RGB/SENTINEL"
# foto.plot()
# foto.plot("/home/benjamin/Documents/PRO/PRODUITS/FOTO_RGB/SENTINEL"
#           "/T23LKC_20201006T132241_B04_method=block_wsize=19_dc=True_foto_data.h5",
#           nb_quadrants=10, norm_method="max")
foto.save_rgb()
# foto.save_data()
